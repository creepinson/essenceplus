package me.creepinson.mod.world.generation;

import java.util.List;
import java.util.Random;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.structure.MapGenStructureIO;
import net.minecraft.world.gen.structure.StructureBoundingBox;
import net.minecraft.world.gen.structure.StructureComponent;
import net.minecraft.world.gen.structure.StructureVillagePieces.Church;
import net.minecraft.world.gen.structure.StructureVillagePieces.Field1;
import net.minecraft.world.gen.structure.StructureVillagePieces.Field2;
import net.minecraft.world.gen.structure.StructureVillagePieces.Hall;
import net.minecraft.world.gen.structure.StructureVillagePieces.House1;
import net.minecraft.world.gen.structure.StructureVillagePieces.House2;
import net.minecraft.world.gen.structure.StructureVillagePieces.House3;
import net.minecraft.world.gen.structure.StructureVillagePieces.House4Garden;
import net.minecraft.world.gen.structure.StructureVillagePieces.Path;
import net.minecraft.world.gen.structure.StructureVillagePieces.PieceWeight;
import net.minecraft.world.gen.structure.StructureVillagePieces.Start;
import net.minecraft.world.gen.structure.StructureVillagePieces.Village;
import net.minecraft.world.gen.structure.StructureVillagePieces.WoodHut;
import net.minecraft.world.gen.structure.template.TemplateManager;
import net.minecraftforge.fml.common.registry.VillagerRegistry;
import net.minecraftforge.fml.common.registry.VillagerRegistry.IVillageCreationHandler;

/**
 * 
 * @author emoniph Updated from WITCHERY mod by Creepinson
 *
 */

public class WorldHandlerVillageDistrict implements IVillageCreationHandler {

	private final Class pieceClazz;
	private final int weight;
	private final int quantityMin;
	private final int quantityMax;

	public WorldHandlerVillageDistrict(Class clazz, int weight, int min) {
		this(clazz, weight, min, min);
	}

	public WorldHandlerVillageDistrict(Class clazz, int weight, int min, int max) {
		this.pieceClazz = clazz;
		this.weight = weight;
		this.quantityMin = min;
		this.quantityMax = max;
	}

	public PieceWeight getVillagePieceWeight(Random rand, int size) {
		return new PieceWeight(this.pieceClazz, this.weight, this.quantityMax <= this.quantityMin ? this.quantityMin
				: this.quantityMin + rand.nextInt(this.quantityMax - this.quantityMin + 1));
	}

	@Override
	public Village buildComponent(PieceWeight villagePiece, Start startPiece, List<StructureComponent> pieces,
			Random rand, int p1, int p2, int p3, EnumFacing p4, int p5) {
		Village object = null;
		if (this.pieceClazz == House4Garden.class) {
			object = House4Garden.createPiece(startPiece, pieces, rand, p1, p2, p3, p4, p5);
		} else if (this.pieceClazz == Church.class) {
			object = Church.createPiece(startPiece, pieces, rand, p1, p2, p3, p4, p5);
		} else if (this.pieceClazz == House1.class) {
			object = House1.createPiece(startPiece, pieces, rand, p1, p2, p3, p4, p5);
		} else if (this.pieceClazz == WoodHut.class) {
			object = WoodHut.createPiece(startPiece, pieces, rand, p1, p2, p3, p4, p5);
		} else if (this.pieceClazz == Hall.class) {
			object = Hall.createPiece(startPiece, pieces, rand, p1, p2, p3, p4, p5);
		} else if (this.pieceClazz == Field1.class) {
			object = Field1.createPiece(startPiece, pieces, rand, p1, p2, p3, p4, p5);
		} else if (this.pieceClazz == Field2.class) {
			object = Field2.createPiece(startPiece, pieces, rand, p1, p2, p3, p4, p5);
		} else if (this.pieceClazz == House2.class) {
			object = House2.createPiece(startPiece, pieces, rand, p1, p2, p3, p4, p5);
		} else if (this.pieceClazz == House3.class) {
			object = House3.createPiece(startPiece, pieces, rand, p1, p2, p3, p4, p5);
		} else if (this.pieceClazz == WorldHandlerVillageDistrict.Wall.class) {
			object = WorldHandlerVillageDistrict.Wall.func_74921_a(startPiece, pieces, rand, p1, p2, p3, p4, p5);
		}

		return object;
	}

	public Class getComponentClass() {
		return this.pieceClazz;
	}

	public static void registerComponent(Class clazz, int weight, int min, int max) {
		VillagerRegistry.instance()
				.registerVillageCreationHandler(new WorldHandlerVillageDistrict(clazz, weight, min, max));
	}

	public static void preInit() {
		try {
			MapGenStructureIO.registerStructureComponent(WorldHandlerVillageDistrict.Wall.class,
					"essenceplus:villagewall");
		} catch (Throwable var4) {

		}

	}

	public static class Wall extends Village {

		private Start start;
		private List pieces;
		private boolean hasMadeWallBlock;

		public Wall() {
		}

		public Wall(Start start, int componentType, Random rand, StructureBoundingBox bounds, EnumFacing baseMode) {
			super(start, componentType);
			super.setCoordBaseMode(baseMode);
			super.boundingBox = bounds;
			this.start = start;
		}

		public static WorldHandlerVillageDistrict.Wall func_74921_a(Start startPiece, List pieces, Random rand, int p1,
				int p2, int p3, EnumFacing p4, int p5) {
			StructureBoundingBox bounds = StructureBoundingBox.getComponentToAddBoundingBox(p1, p2, p3, 0, 0, 0, 2, 7,
					2, p4);
			boolean create = canVillageGoDeeper(bounds) && StructureComponent.findIntersecting(pieces, bounds) == null
					&& !containsWalls(pieces);
			return create ? new WorldHandlerVillageDistrict.Wall(startPiece, p5, rand, bounds, p4) : null;
		}

		private static boolean containsWalls(List pieces2) {
			return false;
		}

		@Override
		public void buildComponent(StructureComponent component, List<StructureComponent> pieces, Random rand) {
	        super.buildComponent(component, pieces, rand);
			this.pieces = pieces;
		}
		
		public boolean addComponentParts(World world, Random rand, StructureBoundingBox bounds) {
			if (super.averageGroundLvl < 0) {
				super.averageGroundLvl = this.getAverageGroundLevel(world, bounds);
				if (super.averageGroundLvl < 0) {
					return true;
				}

				super.boundingBox.offset(0, super.averageGroundLvl - super.boundingBox.maxY + 7 - 1, 0);
			}

			if (!this.hasMadeWallBlock) {
				byte x = 1;
				byte z = 1;
				int xCoord = this.getXWithOffset(x, z);
				int yCoord = this.getYWithOffset(1);
				int zCoord = this.getZWithOffset(x, z);
				if (this.pieces != null && bounds.isVecInside(new Vec3i(xCoord, yCoord, zCoord))) {
					this.hasMadeWallBlock = true;
					WorldHandlerVillageDistrict.Wall.placeWalls(world, xCoord, yCoord, zCoord,
							world.getBiome(new BlockPos(xCoord, yCoord, zCoord)), false);
				}
			}

			return true;
		}

		protected void writeStructureToNBT(NBTTagCompound nbtRoot) {
			super.writeStructureToNBT(nbtRoot);
			nbtRoot.setBoolean("WallBlock", this.hasMadeWallBlock);
		}

		@Override
		protected void readStructureFromNBT(NBTTagCompound tagCompound, TemplateManager p_143011_2_) {
			super.readStructureFromNBT(tagCompound, p_143011_2_);
			this.hasMadeWallBlock = tagCompound.getBoolean("WallBlock");
		}

		public static void placeWalls(World world, int xCoord, int yCoord, int zCoord, Biome biome, boolean desert) {
			int minX = Integer.MAX_VALUE;
			int minZ = Integer.MAX_VALUE;
			int maxX = Integer.MIN_VALUE;
			int maxZ = Integer.MIN_VALUE;

			if (maxX != Integer.MIN_VALUE && minX != Integer.MAX_VALUE && maxZ != Integer.MIN_VALUE
					&& minZ != Integer.MAX_VALUE) {
				byte[][] var45 = new byte[maxX - minX + 3][maxZ - minZ + 3];
				short[][] b = new short[maxX - minX + 3][maxZ - minZ + 3];

				int blockBase;
				int blockFence;
				int blockBaseMeta;
				int stairsBlock;
				int x;
				byte var46 = 7;

				for (blockBase = 1; blockBase < var45.length - var46; ++blockBase) {
					for (blockFence = 1; blockFence < var45[blockBase].length - var46; ++blockFence) {
						if (var45[blockBase][blockFence] == 2) {
							for (stairsBlock = 1; stairsBlock < var46; ++stairsBlock) {
								if (var45[blockBase + stairsBlock][blockFence] == 2
										&& var45[blockBase + stairsBlock - 1][blockFence] == 0) {
									for (blockBaseMeta = stairsBlock; blockBaseMeta > 0; --blockBaseMeta) {
										var45[blockBase + blockBaseMeta][blockFence] = 2;
									}
								}

								if (var45[blockBase][blockFence + stairsBlock] == 2
										&& var45[blockBase][blockFence + stairsBlock - 1] == 0) {
									for (blockBaseMeta = stairsBlock; blockBaseMeta > 0; --blockBaseMeta) {
										var45[blockBase][blockFence + blockBaseMeta] = 2;
									}
								}
							}
						}
					}
				}

				boolean n;
				for (blockBase = 1; blockBase < var45.length - 1; ++blockBase) {
					for (blockFence = 1; blockFence < var45[blockBase].length - 1; ++blockFence) {
						boolean var50 = var45[blockBase][blockFence - 1] == 0;
						boolean var53 = var45[blockBase][blockFence + 1] == 0;
						boolean var52 = var45[blockBase + 1][blockFence] == 0;
						boolean var56 = var45[blockBase - 1][blockFence] == 0;
						boolean var54 = var45[blockBase + 1][blockFence - 1] == 0;
						boolean var58 = var45[blockBase - 1][blockFence + 1] == 0;
						boolean z = var45[blockBase + 1][blockFence + 1] == 0;
						n = var45[blockBase - 1][blockFence - 1] == 0;
						if (!var50 && !var53 && !var52 && !var56 && !var54 && !z && !n && !var58) {
							var45[blockBase][blockFence] = 1;
						}
					}
				}

				IBlockState var48 = Blocks.STONEBRICK.getDefaultState();
				IBlockState var47 = Blocks.OAK_FENCE.getDefaultState();
				IBlockState var49 = Blocks.STONE_BRICK_STAIRS.getDefaultState();
				blockBaseMeta = 0;

				for (x = 1; x < var45.length - 1; ++x) {
					for (int var57 = 1; var57 < var45[x].length - 1; ++var57) {
						n = var45[x][var57 - 1] >= 2;
						boolean s = var45[x][var57 + 1] >= 2;
						boolean e = var45[x + 1][var57] >= 2;
						boolean w = var45[x - 1][var57] >= 2;
						boolean ne = var45[x + 1][var57 - 1] >= 2;
						boolean sw = var45[x - 1][var57 + 1] >= 2;
						boolean se = var45[x + 1][var57 + 1] >= 2;
						boolean nw = var45[x - 1][var57 - 1] >= 2;
						if (var45[x][var57] >= 2) {
							int dx = minX + x;
							int dz = minZ + var57;
							int solidCount = 0;

							int dy;
							int startY;
							for (dy = yCoord; dy > 1 && solidCount < 9; --dy) {
								solidCount = 0;

								for (int minHeight = dx - 1; minHeight <= dx + 1; ++minHeight) {
									for (startY = dz - 1; startY <= dz + 1; ++startY) {
										IBlockState near = world.getBlockState(new BlockPos(minHeight, dy, startY));
										boolean lowestY = near.getMaterial().isReplaceable()
												|| near.getMaterial() == Material.LEAVES
												|| near.getMaterial() == Material.WOOD
												|| near.getMaterial() == Material.PLANTS;
										if (near.isNormalCube() && !lowestY) {
											++solidCount;
										}
									}
								}
							}

							boolean var59 = true;
							startY = dy + 9;
							int var61 = Math.max(Math.max(Math.max(b[x - 1][var57], b[x + 1][var57]), b[x][var57 + 1]),
									b[x][var57 - 1]);
							if (var61 > 0) {
								if (var61 > startY) {
									startY = var61 - 1;
								} else if (var61 < startY) {
									startY = var61 + 1;
								}
							}

							int var60 = dy;
							if (startY - dy > 0) {
								b[x][var57] = (short) Math.min(Math.max(startY, 0), 32767);
							}

							for (dy = startY; dy > var60; --dy) {
								if (dy == startY) {
									if (!ne && !n && !e) {
										setBlock(world, dx + 2, dy, dz - 2, var48);
										setBlock(world, dx + 2, dy, dz - 1, var48);
										setBlock(world, dx + 1, dy, dz - 2, var48);
										setBlock(world, dx + 2, dy + 1, dz - 2, var48, false);
										setBlock(world, dx + 2, dy + 1, dz - 1, var48, false);
										setBlock(world, dx + 1, dy + 1, dz - 2, var48, false);
									}

									if (!nw && !n && !w) {
										setBlock(world, dx - 2, dy, dz - 2, var48);
										setBlock(world, dx - 1, dy, dz - 2, var48);
										setBlock(world, dx - 2, dy, dz - 1, var48);
										setBlock(world, dx - 2, dy + 1, dz - 2, var48, false);
										setBlock(world, dx - 1, dy + 1, dz - 2, var48, false);
										setBlock(world, dx - 2, dy + 1, dz - 1, var48, false);
									}

									if (!se && !s && !e) {
										setBlock(world, dx + 2, dy, dz + 2, var48);
										setBlock(world, dx + 1, dy, dz + 2, var48);
										setBlock(world, dx + 2, dy, dz + 1, var48);
										setBlock(world, dx + 2, dy + 1, dz + 2, var48, false);
										setBlock(world, dx + 1, dy + 1, dz + 2, var48, false);
										setBlock(world, dx + 2, dy + 1, dz + 1, var48, false);
									}

									if (!sw && !s && !w) {
										setBlock(world, dx - 2, dy, dz + 2, var48);
										setBlock(world, dx - 1, dy, dz + 2, var48);
										setBlock(world, dx - 2, dy, dz + 1, var48);
										setBlock(world, dx - 2, dy + 1, dz + 2, var48, false);
										setBlock(world, dx - 1, dy + 1, dz + 2, var48, false);
										setBlock(world, dx - 2, dy + 1, dz + 1, var48, false);
									}

									if (!n && !ne && !nw) {
										setBlock(world, dx, dy, dz - 2, var48);
										setBlock(world, dx, dy + 1, dz - 2, var49, false);
									}

									if (!e && !se && !ne) {
										setBlock(world, dx + 2, dy, dz, var48);
										setBlock(world, dx + 2, dy + 1, dz, var49, false);
									}

									if (!s && !se && !sw) {
										setBlock(world, dx, dy, dz + 2, var48);
										setBlock(world, dx, dy + 1, dz + 2, var49, false);
									}

									if (!w && !nw && !sw) {
										setBlock(world, dx - 2, dy, dz, var48);
										setBlock(world, dx - 2, dy + 1, dz, var49, false);
									}

								} else {
									byte distCheck = 4;
									boolean gate = var45[x][var57] == 3 && (x > distCheck
											&& x < var45.length - distCheck && var45[x - distCheck][var57] == 2
											&& var45[x + distCheck][var57] == 2
											|| var57 > distCheck && var57 < var45[x].length - distCheck
													&& var45[x][var57 - distCheck] == 2
													&& var45[x][var57 + distCheck] == 2);
									if (gate && dy == startY - 3) {
										world.setBlockState(new BlockPos(dx, dy, dz), var47);
										if (var45[x + 1][var57] != 3 || var45[x - 1][var57] != 3) {
											if (var45[x + 1][var57] == 3) {
												world.setBlockState(new BlockPos(dx, dy, dz - 1), var49, 2);
												world.setBlockState(new BlockPos(dx, dy, dz + 1), var49, 2);
											} else if (var45[x - 1][var57] == 3) {
												world.setBlockState(new BlockPos(dx, dy, dz - 1), var49, 2);
												world.setBlockState(new BlockPos(dx, dy, dz + 1), var49, 2);
											} else if (var45[x][var57 + 1] != 3 || var45[x][var57 - 1] != 3) {
												if (var45[x][var57 - 1] == 3) {
													world.setBlockState(new BlockPos(dx - 1, dy, dz), var49, 2); // meta
																													// 6
													world.setBlockState(new BlockPos(dx + 1, dy, dz), var49, 2);
												} else if (var45[x][var57 + 1] == 3) {
													world.setBlockState(new BlockPos(dx - 1, dy, dz), var49, 2); // meta
																													// 7
													world.setBlockState(new BlockPos(dx + 1, dy, dz), var49, 2);
												}
											}
										}
									}

									if (!gate || dy > startY - 3) {
										setBlock(world, dx, dy, dz, var48);
										boolean ng = var45[x][var57 - 1] == 3;
										boolean sg = var45[x][var57 + 1] == 3;
										boolean eg = var45[x + 1][var57] == 3;
										boolean wg = var45[x - 1][var57] == 3;
										if (!ng) {
											setBlock(world, dx, dy, dz - 1, var48);
										}

										if (!ng && !eg) {
											setBlock(world, dx + 1, dy, dz - 1, var48);
										}

										if (!ng && !wg) {
											setBlock(world, dx - 1, dy, dz - 1, var48);
										}

										if (!eg) {
											setBlock(world, dx + 1, dy, dz, var48);
										}

										if (!sg) {
											setBlock(world, dx, dy, dz + 1, var48);
										}

										if (!sg && !eg) {
											setBlock(world, dx + 1, dy, dz + 1, var48);
										}

										if (!sg && !wg) {
											setBlock(world, dx - 1, dy, dz + 1, var48);
										}

										if (!wg) {
											setBlock(world, dx - 1, dy, dz, var48);
										}
									}
								}
							}
						}
					}
				}
			}

		}

	}

	private static void setBlock(World world, int x, int y, int z, IBlockState block) {
		setBlock(world, x, y, z, block, true);
	}

	private static void setBlock(World world, int x, int y, int z, IBlockState block, boolean notStacked) {
		IBlockState replaceBlock = world.getBlockState(new BlockPos(x, y, z));
		Material material = replaceBlock.getMaterial();
		if (material.isReplaceable() || material == Material.LEAVES || material == Material.WOOD
				|| material == Material.PLANTS) {
			world.setBlockState(new BlockPos(x, y, z), block, 2);
		}

	}

	public static class StructureBounds extends StructureBoundingBox {

		public final boolean ew;

		public StructureBounds(Path path, int expansionX, int expansionZ) {
			this(path.getBoundingBox(), expansionX, expansionZ);
		}

		public StructureBounds(StructureBoundingBox bb, int expansionX, int expansionZ) {
			this(bb.minX, bb.minY, bb.minZ, bb.maxX, bb.maxY, bb.maxZ, expansionX, expansionZ);
		}

		public StructureBounds(int x, int y, int z, int x2, int y2, int z2, int expansionX, int expansionZ) {
			this.ew = x2 - x > z2 - z;
			if (this.ew) {
				super.minX = x - expansionZ;
				super.maxX = x2 + expansionZ;
				super.minZ = z - expansionX;
				super.maxZ = z2 + expansionX;
			} else {
				super.minX = x - expansionX;
				super.maxX = x2 + expansionX;
				super.minZ = z - expansionZ;
				super.maxZ = z2 + expansionZ;
			}

			super.minY = y;
			super.maxY = y2;
		}
	}
}
