package me.creepinson.mod.world.dimension;

import me.creepinson.mod.world.biome.EPBiomeRegistry;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.DimensionType;
import net.minecraft.world.WorldProvider;
import net.minecraft.world.biome.BiomeProvider;
import net.minecraft.world.biome.BiomeProviderSingle;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class DimensionTypeCreepola extends WorldProvider {

	@Override
	public BiomeProvider getBiomeProvider() {
		this.biomeProvider = new BiomeProviderSingle(EPBiomeRegistry.creepop);
		this.hasSkyLight = true;
		return this.biomeProvider;
	}


	
	
	@Override
	public DimensionType getDimensionType() {

		return EPDimensionRegistry.CREEPOLA_NORMAL;
	}

	@Override
	public boolean isSurfaceWorld() {

		return true;

	}

	@Override
	public boolean canCoordinateBeSpawn(int par1, int par2) {

		return true;

	}

	@Override
	public boolean canRespawnHere() {

		return false;

	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean doesXZShowFog(int x, int z) {
		return false;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public Vec3d getFogColor(float par1, float par2) {

		return new Vec3d(0.6D, 0.0D, 0.7D);

	}


	/*@Override
	public IChunkGenerator createChunkGenerator() {
		return new CreepolaChunkGenerator(this.world, this.getSeed(), "");
	}*/

	
}
