package me.creepinson.mod.util.world;

import java.util.Random;

import me.creepinson.creepinoutils.util.CreepinoUtils;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;

public class WorldGenBlocks extends WorldGenerator {
    private static final IBlockState DEFAULT_BLOCK = Blocks.STONE.getDefaultState();
    private final int minWidth;
    private final int minHeight;
    private final int minDepth;

    private final IBlockState block;

    public WorldGenBlocks() {
        this(1, 1, 1, DEFAULT_BLOCK);
    }

    public WorldGenBlocks(int minWidth, int minHeight, int minDepth, IBlockState mainblock) {
        super();
        this.minWidth = minWidth;
        this.minHeight = minHeight;
        this.minDepth = minDepth;
        this.block = mainblock;
    }


    public boolean generate(World worldIn, Random rand, BlockPos position) {
        boolean flag = true;
        BlockPos pos = CreepinoUtils.getGround(worldIn, position);
        if (pos.getY() != worldIn.getHeight()) {
            for (int x = pos.getX(); x < minWidth; x++) {
                for (int y = pos.getY(); y < minHeight; y++) {
                    for (int z = pos.getZ(); z < minDepth; z++) {
                        worldIn.setBlockState(new BlockPos(x, y, z), this.block);
                        flag = true;
                    }
                }
            }
        } else {
            flag = false;
        }
        return flag;
    }

    public boolean generate(World worldIn, Random rand) {
        boolean flag = true;
        BlockPos tmp = new BlockPos(rand.nextInt(), rand.nextInt(), rand.nextInt());
        BlockPos pos = CreepinoUtils.getGround(worldIn, tmp);
        if (pos.getY() != worldIn.getHeight()) {
            worldIn.setBlockState(pos, this.block);
            flag = true;
        } else {
            flag = false;
        }
        return flag;
    }


}
