package me.creepinson.mod.entity;

import me.creepinson.mod.util.ShapeTypes;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class EntityColoredShape extends EntityThrowable {
	public ShapeTypes type = ShapeTypes.LIME_TRIANGLE;

	public EntityColoredShape(World worldIn) {
		super(worldIn);
		this.setSize(0.25F, 0.25F);
		this.type = ShapeTypes.LIME_TRIANGLE;
	}

	public EntityColoredShape(World worldIn, double x, double y, double z, ShapeTypes type) {
		super(worldIn);

		this.type = type;
		if(this.type == ShapeTypes.LIME_TRIANGLE) {
			this.setSize(2.5F, 2.5F);
		} else {
			this.setSize(0.5F, 0.5F);
		}
		this.setPosition(x, y, z);
	}

	@Override
	protected void onImpact(RayTraceResult result) {
		if (result.entityHit != null) {
			if (this.type == ShapeTypes.LIME_TRIANGLE) {
				result.entityHit.attackEntityFrom(DamageSource.GENERIC, 10F);
				for (int i = 0; i < 5; i++) {
					this.getEntityWorld().spawnParticle(EnumParticleTypes.SLIME, this.posX, this.posY, this.posZ, 0.0D,
							0.0D, 0.0D);
				}
			}
		}
	}
}