package me.creepinson.mod.entity.boss;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import me.creepinson.mod.core.EssencePlus;
import me.creepinson.mod.entity.EntityCreepino;
import me.creepinson.mod.util.ModUtils;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILeapAtTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.BossInfo;
import net.minecraft.world.BossInfoServer;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityCreeperinoBoss extends EntityMob {
	private final BossInfoServer bossInfo = (BossInfoServer) (new BossInfoServer(this.getDisplayName(),
			BossInfo.Color.PURPLE, BossInfo.Overlay.PROGRESS)).setDarkenSky(true);

	private static final DataParameter<Integer> SPOT_COLOR = EntityDataManager.<Integer>createKey(EntityCreepino.class,
			DataSerializers.VARINT);
	private static final String[] TEXTURES = new String[] { ModUtils.MOD_ID + "textures/entity/creepino/creepino_white.png",
			ModUtils.MOD_ID + "textures/entity/creepino/creepino_red.png" };
	private String texturePrefix;
	private static final String[] TEXTURES_ABBR = new String[] { "cw", "cr" };

	public int getTextureType() {
		return this.dataManager.get(SPOT_COLOR);
	}

	@Override
	public ITextComponent getDisplayName() {
		return new TextComponentString("Creeperino");
	}

	World world = null;

	public void addTrackingPlayer(EntityPlayerMP player) {
		super.addTrackingPlayer(player);
		this.bossInfo.addPlayer(player);
	}

	/**
	 * Removes the given player from the list of players tracking this entity. See
	 * {@link Entity#addTrackingPlayer} for more information on tracking.
	 */
	public void removeTrackingPlayer(EntityPlayerMP player) {
		super.removeTrackingPlayer(player);
		this.bossInfo.removePlayer(player);
	}

	@Override
	protected void updateAITasks() {
		super.updateAITasks();
		List<EntityPlayerMP> trackingList = this.world.getEntitiesWithinAABB(EntityPlayerMP.class,
				new AxisAlignedBB(this.getPosition()));
		for (EntityPlayer p : trackingList) {
			this.addTrackingPlayer((EntityPlayerMP) p);
		}

		this.bossInfo.setPercent(this.getHealth() / this.getMaxHealth());
	}
	
	@Override
	public float getRenderSizeModifier() {
		return 2.0F;
	}

	public EntityCreeperinoBoss(World var1) {
		super(var1);
		world = var1;
		this.isImmuneToFire = true;
		addRandomArmor();
		this.setSize(2, 4);
		this.tasks.addTask(0, new EntityAISwimming(this));
		this.tasks.addTask(6, new EntityAIWander(this, 1.0D));
		this.tasks.addTask(8, new EntityAILookIdle(this));
		this.tasks.addTask(2, new EntityAIWatchClosest(this, EntityPlayer.class, 6.0F));
		this.tasks.addTask(3, new EntityAIAttackMelee(this, 1.0D, false));
		this.tasks.addTask(4, new EntityAILeapAtTarget(this, 0.42F));
		this.targetTasks.addTask(10,
				new EntityAINearestAttackableTarget<EntityPlayerMP>(this, EntityPlayerMP.class, true));
		this.targetTasks.addTask(10, new EntityAINearestAttackableTarget<EntityPlayer>(this, EntityPlayer.class, true));
		this.targetTasks.addTask(5, new EntityAIHurtByTarget(this, false));
		this.tasks.addTask(1, new EntityAILookIdle(this));
		this.tasks.addTask(1, new EntityAIWander(this, 0.8D));
		this.tasks.addTask(6, new EntityAIAttackMelee(this, 1.0D, false));

	}

	public void setSpot(int variant) {
		this.dataManager.set(SPOT_COLOR, Integer.valueOf(variant));
	}

	public int getSpot() {
		return ((Integer) this.dataManager.get(SPOT_COLOR)).intValue();
	}

	protected void entityInit() {
		super.entityInit();
		int n = ThreadLocalRandom.current().nextInt(0, 1 + 1);

		this.dataManager.register(SPOT_COLOR, 0);
		this.setSpot(n);
	}

	public void writeEntityToNBT(NBTTagCompound compound) {
		super.writeEntityToNBT(compound);
		compound.setInteger("Variant", this.getSpot());
	}

	public void readEntityFromNBT(NBTTagCompound compound) {
		super.readEntityFromNBT(compound);
		this.setSpot(compound.getInteger("Variant"));
	}

	private void resetTexturePrefix() {
		this.texturePrefix = null;
	}

	@SideOnly(Side.CLIENT)
	private void setTexturePaths() {
		int i = this.getSpot();

		this.texturePrefix = TEXTURES[i];

	}

	@SideOnly(Side.CLIENT)
	public String getTexture() {
		if (this.texturePrefix == null) {
			this.setTexturePaths();
		}

		return this.texturePrefix;
	}

	protected void applyEntityAttributes() {
		super.applyEntityAttributes();
		this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.375D);
		this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(1000D);
		if (this.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE) != null)
			this.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(3.9D);
	}

	protected void addRandomArmor() {

	}

	public void onLivingUpdate() {

		super.onLivingUpdate();
		World world = this.world;
		int x = (int) this.posX;
		int y = (int) this.posY;
		int z = (int) this.posZ;
		Random ran = this.rand;
		for (int l = 0; l < 6; ++l) {
			world.spawnParticle(EnumParticleTypes.SLIME, x, y, z, 0.0D, 0.0D, 0.0D);
		}
	}

	@Override
	protected ResourceLocation getLootTable() {
		return new ResourceLocation(ModUtils.MOD_ID, "creepino_loot");

	}

	@Override
	protected net.minecraft.util.SoundEvent getAmbientSound() {
		return EssencePlus.CREEPERINO_LIVING;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn) {
		return (net.minecraft.util.SoundEvent) SoundEvents.ENTITY_ENDERDRAGON_GROWL;

	}

	@Override
	protected net.minecraft.util.SoundEvent getDeathSound() {
		return EssencePlus.CREEPERINO_DEATH;
	}

	@Override
	public void onStruckByLightning(EntityLightningBolt entityLightningBolt) {
		super.onStruckByLightning(entityLightningBolt);
		int i = (int) this.posX;
		int j = (int) this.posY;
		int k = (int) this.posZ;
		Entity entity = this;

		j += 1;

	}

	@Override
	public void fall(float l, float d) {
		super.fall(l, d);
		int i = (int) this.posX;
		int j = (int) this.posY;
		int k = (int) this.posZ;
		super.fall(l, d);
		Entity entity = this;

	}

	@Override
	public void onDeath(DamageSource source) {
		super.onDeath(source);
		int i = (int) this.posX;
		int j = (int) this.posY;
		int k = (int) this.posZ;
		Entity entity = this;

	}

	@Override
	protected boolean processInteract(EntityPlayer entity, EnumHand hand) {
		super.processInteract(entity, hand);
		int i = (int) this.posX;
		int j = (int) this.posY;
		int k = (int) this.posZ;

		return true;
	}

	@Override
	protected float getSoundVolume() {
		return 0.5F;
	}

	/**
	 * CreeprBoss - Creepinson Created using Tabula 7.0.0
	 */
	public static class ModelCreeperinoBoss extends ModelBase {
		public double[] modelScale = new double[] { 10, 10, 10 };
		public ModelRenderer Body;
		public ModelRenderer Leg;
		public ModelRenderer Head;
		public ModelRenderer Arm1;
		public ModelRenderer Arm2;

		public ModelCreeperinoBoss() {
			this.textureWidth = 128;
			this.textureHeight = 128;
			this.Head = new ModelRenderer(this, 0, 8);
			this.Head.setRotationPoint(-3.0F, -11.2F, -3.0F);
			this.Head.addBox(0.0F, 0.0F, 0.0F, 6, 6, 6, 0.0F);
			this.setRotateAngle(Head, 0.0F, -0.03717551306747922F, 0.0F);
			this.Arm1 = new ModelRenderer(this, 39, 44);
			this.Arm1.setRotationPoint(1.0F, 0.0F, -2.0F);
			this.Arm1.addBox(0.0F, 0.0F, 0.0F, 4, 15, 4, -0.5F);
			this.setRotateAngle(Arm1, -1.2749428053995138F, 0.0F, -1.2042771838760875F);
			this.Body = new ModelRenderer(this, 92, 0);
			this.Body.setRotationPoint(-4.0F, -5.2F, -4.0F);
			this.Body.addBox(0.0F, 0.0F, 0.0F, 8, 14, 8, 0.0F);
			this.Leg = new ModelRenderer(this, 39, 44);
			this.Leg.setRotationPoint(-2.0F, 9.0F, -2.0F);
			this.Leg.addBox(0.0F, 0.0F, 0.0F, 4, 15, 4, 0.6F);
			this.Arm2 = new ModelRenderer(this, 39, 44);
			this.Arm2.setRotationPoint(-3.0F, -4.0F, -2.0F);
			this.Arm2.addBox(0.0F, 0.0F, 0.0F, 4, 15, 4, -0.5F);
			this.setRotateAngle(Arm2, -1.3613568165555772F, 0.3673918075448064F, 0.17191493132144145F);
		}

		@Override
		public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
			GlStateManager.pushMatrix();
			GlStateManager.scale(modelScale[0], modelScale[1], modelScale[2]);
			GlStateManager.popMatrix();
			this.Head.render(f5);
			this.Arm1.render(f5);
			this.Body.render(f5);
			this.Leg.render(f5);
			this.Arm2.render(f5);
		}

		/**
		 * This is a helper function from Tabula to set the rotation of model parts
		 */
		public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

	}

}