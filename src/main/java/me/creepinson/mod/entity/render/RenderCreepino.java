package me.creepinson.mod.entity.render;

import me.creepinson.mod.entity.EntityCreepino;
import me.creepinson.mod.util.ModUtils;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;

public class RenderCreepino extends RenderLiving<EntityCreepino> {

	public RenderCreepino(RenderManager rendermanagerIn, ModelBase modelbaseIn, float shadowsizeIn) {
		super(rendermanagerIn, modelbaseIn, shadowsizeIn);

	}

	private final ResourceLocation creepinoWhite = new ResourceLocation(ModUtils.MOD_ID,
			"textures/entity/creepino/creepino_white.png");

	private final ResourceLocation creepinoRed = new ResourceLocation(ModUtils.MOD_ID,
			"textures/entity/creepino/creepino_red.png");

	protected ResourceLocation getEntityTexture(EntityCreepino entity) {
		EntityCreepino thisentity = (EntityCreepino) entity;
		switch (entity.getTextureType()) {
		case 0:
			return creepinoWhite;
		case 1:
			return creepinoRed;
		default:
			return creepinoRed;
		}

	}

}
