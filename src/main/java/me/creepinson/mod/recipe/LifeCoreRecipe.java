package me.creepinson.mod.recipe;

import me.creepinson.mod.core.ItemHandler;
import me.creepinson.mod.util.ModUtils;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.oredict.ShapedOreRecipe;

import javax.annotation.Nonnull;

public class LifeCoreRecipe extends ShapedOreRecipe {
    public LifeCoreRecipe(boolean zombie) {
        super(new ResourceLocation(ModUtils.MOD_ID, "core"), ItemStack.EMPTY, new String[]{
                " B ",
                "AXA",
                " B "
        }, 'B', Ingredient.fromStacks(new ItemStack(ItemHandler.largeBone)), 'A', Ingredient.fromStacks(new ItemStack(ItemHandler.essence, 1, 2)), 'X', zombie ? Ingredient.fromStacks(new ItemStack(ItemHandler.syringe, 1, 1)) : Ingredient.fromStacks(new ItemStack(ItemHandler.syringe, 1, 2)));
        setRegistryName(zombie ? "core_life_zombie" : "core_life");
    }

    @Nonnull
    @Override
    public ItemStack getCraftingResult(@Nonnull InventoryCrafting var1) {
        ItemStack stack = new ItemStack(ItemHandler.core, 1, 2);
        ItemStack syringe = var1.getStackInRowAndColumn(1, 2);
        stack.setTagCompound(syringe.getTagCompound() != null ? syringe.getTagCompound() : new NBTTagCompound());
        return stack;
    }
}
