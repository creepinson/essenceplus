package me.creepinson.mod.block;

import me.creepinson.mod.util.ModUtils;
import net.minecraft.block.BlockFire;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.ResourceLocation;

public class BlockCreepolaFire extends BlockFire {

    public BlockCreepolaFire(Material mat, String name, CreativeTabs tab) {
        super();
        this.setCreativeTab(tab);
        this.setRegistryName(new ResourceLocation(ModUtils.MOD_ID, name));
    }

}
