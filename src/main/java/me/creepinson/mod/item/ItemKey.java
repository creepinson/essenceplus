package me.creepinson.mod.item;

import me.creepinson.mod.core.EnumHandler;
import me.creepinson.mod.core.EnumHandler.BaseTypes;
import me.creepinson.mod.util.ModUtils;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import me.creepinson.creepinoutils.base.BaseItem;

public class ItemKey extends BaseItem {

	public ItemKey(String name, CreativeTabs tab) {
		super(new ResourceLocation(ModUtils.MOD_ID, name), tab);
		this.setHasSubtypes(true);
	}

	@Override
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
		for (int i = 0; i < BaseTypes.values().length; i++) {
			ItemStack stack = new ItemStack(this, 1, i);
			if (getCreativeTab() != tab)
				return;
			
			items.add(stack);
		}
	}
	@Override
	public String getTranslationKey(ItemStack stack) {
		for (int i = 0; i < BaseTypes.values().length; i++) {
			if (stack.getItemDamage() == i) {
				return EnumHandler.BaseTypes.values()[i].getName() + "key";
			}

			else {
				continue;
			}

		}
		return EnumHandler.BaseTypes.magic.getName() + "key";

	}

}