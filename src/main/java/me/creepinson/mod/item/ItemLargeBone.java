package me.creepinson.mod.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.ResourceLocation;
import me.creepinson.creepinoutils.base.BaseItem;
import me.creepinson.mod.util.ModUtils;

public class ItemLargeBone extends BaseItem {

	public ItemLargeBone(String name, CreativeTabs tab) {
		super(new ResourceLocation(ModUtils.MOD_ID, name), tab);

	}

}