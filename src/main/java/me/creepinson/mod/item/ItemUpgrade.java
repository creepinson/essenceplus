package me.creepinson.mod.item;

import me.creepinson.mod.core.EnumHandler;
import me.creepinson.mod.core.EnumHandler.UpgradeTypes;
import me.creepinson.mod.util.ModUtils;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import me.creepinson.creepinoutils.base.BaseItem;

public class ItemUpgrade extends BaseItem {

	public ItemUpgrade(String name, CreativeTabs tab) {
		super(new ResourceLocation(ModUtils.MOD_ID, name), tab);
		this.setHasSubtypes(true);
	}

	@Override
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
		for (int i = 0; i < UpgradeTypes.values().length; i++) {
			ItemStack stack = new ItemStack(this, 1, i);
			if (getCreativeTab() != tab)
				return;
			
			items.add(stack);
		}
	}
	@Override
	public String getTranslationKey(ItemStack stack) {
		for (int i = 0; i < UpgradeTypes.values().length; i++) {
			if (stack.getItemDamage() == i) {
				return EnumHandler.UpgradeTypes.values()[i].getName() + "upgrade";
			}

			else {
				continue;
			}

		}
		return EnumHandler.UpgradeTypes.locking.getName() + "upgrade";

	}

}