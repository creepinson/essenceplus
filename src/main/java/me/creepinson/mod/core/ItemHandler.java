package me.creepinson.mod.core;

import me.creepinson.creepinoutils.base.BaseItem;
import me.creepinson.mod.core.EnumHandler.BaseTypes;
import me.creepinson.mod.core.EnumHandler.Chips;
import me.creepinson.mod.core.EnumHandler.Circuits;
import me.creepinson.mod.core.EnumHandler.Cores;
import me.creepinson.mod.core.EnumHandler.Essences;
import me.creepinson.mod.core.EnumHandler.SyringeTypes;
import me.creepinson.mod.core.EnumHandler.UpgradeTypes;
import me.creepinson.mod.core.EnumHandler.Wires;
import me.creepinson.mod.item.CreepoMatic;
import me.creepinson.mod.item.ItemChip;
import me.creepinson.mod.item.ItemCircuit;
import me.creepinson.mod.item.ItemCore;
import me.creepinson.mod.item.ItemEssence;
import me.creepinson.mod.item.ItemKey;
import me.creepinson.mod.item.ItemLargeBone;
import me.creepinson.mod.item.ItemLimeTriangle;
import me.creepinson.mod.item.ItemStickOfLightning;
import me.creepinson.mod.item.ItemSyringe;
import me.creepinson.mod.item.ItemUpgrade;
import me.creepinson.mod.item.ItemWire;
import me.creepinson.mod.util.ModUtils;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.IForgeRegistry;

public class ItemHandler {

    public static BaseItem stickOfLightning;

    // ESSENCEPLUS SCIENCE
    public static BaseItem largeBone;
    public static BaseItem syringe;

    // ESSENCEPLUS TECH
    public static BaseItem chip;
    public static BaseItem wire;
    public static BaseItem circuit;
    public static BaseItem ingotCopper;
    public static Item upgrade;
    public static Item key;

    // ESSENCEPLUS BASE
    public static BaseItem core;
    public static BaseItem essence;
    public static BaseItem buttKicker;
    public static BaseItem limeTriangle;
    public static CreepoMatic creepoMatic;

    public static void init() {
        creepoMatic = new CreepoMatic("creepomatic", EssencePlus.instance.creativeTab);
        essence = new ItemEssence("essence", EssencePlus.instance.creativeTab);
        core = new ItemCore("core", EssencePlus.instance.creativeTab);
        ingotCopper = new BaseItem(new ResourceLocation(ModUtils.MOD_ID, "ingot_copper"))
                .setCreativeTab(CreativeTabs.MATERIALS);
        limeTriangle = new ItemLimeTriangle("lime_triangle");
        stickOfLightning = new ItemStickOfLightning("stickoflightning", EssencePlus.instance.creativeTab).setMaxStackSize(1);
        largeBone = new ItemLargeBone("large_bone", EssencePlus.instance.creativeTab);
        upgrade = new ItemUpgrade("upgrade", EssencePlus.instance.creativeTab);
        key = new ItemKey("key", EssencePlus.instance.creativeTab);
        chip = new ItemChip("chip", EssencePlus.instance.creativeTab);
        wire = new ItemWire("wire", EssencePlus.instance.creativeTab);
        circuit = new ItemCircuit("circuit", EssencePlus.instance.creativeTab);

        syringe = new ItemSyringe("syringe", EssencePlus.instance.creativeTab).setMaxStackSize(1);
    }

    public static void register(IForgeRegistry<Item> registry) {

        registry.registerAll(creepoMatic, limeTriangle, ingotCopper, essence, core, stickOfLightning, largeBone,
                syringe, chip, circuit, wire);
    }

    public static void registerModels() {
        EssencePlus.registerItemRenderer(ingotCopper, 0, ingotCopper.getRegistryName().getPath());
        EssencePlus.registerItemRenderer(limeTriangle, 0, limeTriangle.getRegistryName().getPath());
        EssencePlus.registerItemRenderer(creepoMatic, 0, creepoMatic.getRegistryName().getPath());
        EssencePlus.registerItemRenderer(stickOfLightning, 0, stickOfLightning.getRegistryName().getPath());
        EssencePlus.registerItemRenderer(largeBone, 0, largeBone.getRegistryName().getPath());

        for (int i = 0; i < SyringeTypes.values().length; i++) {
            EssencePlus.registerItemRenderer(syringe, i, "syringe_" + EnumHandler.SyringeTypes.values()[i].getName());
        }

        for (int i = 0; i < Chips.values().length; i++) {
            EssencePlus.registerItemRenderer(chip, i, "chip" + EnumHandler.Chips.values()[i].getName());
        }

        for (int i = 0; i < Circuits.values().length; i++) {
            EssencePlus.registerItemRenderer(circuit, i, "circuit" + EnumHandler.Circuits.values()[i].getName());
        }

        for (int i = 0; i < Wires.values().length; i++) {
            EssencePlus.registerItemRenderer(wire, i, "wire" + EnumHandler.Wires.values()[i].getName());
        }

        for (int i = 0; i < BaseTypes.values().length; i++) {
            EssencePlus.registerItemRenderer(key, i, EnumHandler.BaseTypes.values()[i].getName() + "key");
        }
        for (int i = 0; i < UpgradeTypes.values().length; i++) {
            EssencePlus.registerItemRenderer(upgrade, i, EnumHandler.UpgradeTypes.values()[i].getName() + "upgrade");
        }

        for (int i = 0; i < Cores.values().length; i++) {
            EssencePlus.registerItemRenderer(core, i, EnumHandler.Cores.values()[i].getName() + "core");
        }
        for (int i = 0; i < Essences.values().length; i++) {
            EssencePlus.registerItemRenderer(essence, i, EnumHandler.Essences.values()[i].getName() + "essence");
        }
    }

    public static void oreDictionary() {
        ingotCopper.registerToOreDictionary("ingotCopper", 0);
    }

}